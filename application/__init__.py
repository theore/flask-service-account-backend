# -*- coding: utf-8 -*-
import logging

from flask import current_app, Flask, redirect, url_for
from flask import render_template

import settings


def create_app(config, debug=False, testing=False, config_overrides=None):
    app = Flask(__name__, template_folder=settings.TEMPLATE_FOLDER)
    app.config.from_object(config)

    app.debug = debug
    app.testing = testing

    if config_overrides:
        app.config.update(config_overrides)

    # Configure logging
    if not app.testing:
        logging.basicConfig(level=logging.INFO)

    # Register any blueprint.
    from application.hello.views import bp_hello
    app.register_blueprint(bp_hello)

    # [START app.route]
    # Add a default root route.
    @app.route("/")
    def index():
        return redirect(url_for('hello.home'))

    @app.route('/_ah/warmup')
    def warmup():
        """App Engine warmup handler
        See http://code.google.com/appengine/docs/python/config/appconfig.html#Warming_Requests

        """
        return 'success'

    @app.route('/_ah/start')
    def start():
        return 'start'

    # Error handlers
    # Handle 404 errors
    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    # Handle 500 errors
    @app.errorhandler(500)
    def server_error(e):
        logging.error("!!!! -- server_error -- 500 Internal Server Error")
        # send_error_mail_for_developer(app.config['SENDER_ADDRESS'],
        #                                         app.config['DEVELOPER_MAILS'],
        #                                         "500 Internal Server Error", e)

        app.logger.error(e)
        return render_template('500.html'), 500
    # [END app.route]

    return app
