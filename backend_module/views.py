# -*- coding: utf-8 -*-
import inspect
import json
import logging
import os

import httplib2
from apiclient.discovery import build
from flask import request, jsonify, Blueprint
from google.appengine.api import memcache
from oauth2client.contrib.appengine import AppAssertionCredentials
from oauth2client.service_account import ServiceAccountCredentials

from settings import SERVICE_ACCOUNT_JSON

bp_backend_module = Blueprint('backend_module', __name__, url_prefix='/backend_module')


@bp_backend_module.route('/', methods=['GET'])
def home():
    return 'Hello Backend.'


@bp_backend_module.route('/calendar', methods=['GET'])
def calendar():
    if os.environ.get('SERVER_SOFTWARE', 'Dev').startswith('Dev'):
        scopes = ['https://www.googleapis.com/auth/calendar']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            SERVICE_ACCOUNT_JSON, scopes)
    else:
        credentials = AppAssertionCredentials(scope='https://www.googleapis.com/auth/calendar')

    calendar_id = 'calendar-id'
    http = credentials.authorize(httplib2.Http(memcache))
    service = build('calendar', 'v3')
    events = service.events().list(
        calendarId=calendar_id,
        singleEvents=True, maxResults=250,
        orderBy='startTime').execute(http=http)

    logging.info(events)
    return jsonify(events)
