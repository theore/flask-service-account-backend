import os

TEMPLATE_FOLDER = os.path.join(os.path.dirname(__file__), 'templates')
SERVICE_ACCOUNT_JSON = os.path.join(os.path.dirname(__file__), 'service-account.json')
